import './App.css'; //Not used, only stays cus I dont want to delete it yet
//Import all pages and things from react router that is needed
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Login from './views/Login';
import Translation from './views/Translation';
import Profile from './views/Profile';
import NotFound from './views/NotFound';

function App() {
  
  return (
    <BrowserRouter>
    <div className="App">
      <Routes>
       <Route> {/*  All the different pages that exist in the app and one if something goes wrong */}
          <Route path = "/" element = { <Login /> }/> {/* The login page */}
          <Route path = "/translations" element = { <Translation /> }/> {/* The translation page */}
          <Route path = "/profile" element = { <Profile /> }/> {/* The profile page */}
          <Route path = "*" element = { <NotFound /> }/> {/* If something dont go to the correct page this page shows up */}
        </Route>
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
