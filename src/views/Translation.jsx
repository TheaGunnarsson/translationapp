
import withAuth from "../hoc/withAuth"
import { NavLink } from "react-router-dom"
import { useState } from "react";
import { PostTranslation } from "../api/index.js";
import { useUser } from '../context/UserContext'
//get local storage user

const Translation = () => {
    const us = useUser();
    //this is the user currently logged in
    //declares state variables
    const [inputText, setInputText] = useState('')
    const [image, setImage] = useState(null) 

    //To handle translations button click
    const ClickTranslateBtn = () =>{
        //check requirements, translate, add to api
        //if empty
        if(inputText==""){
            alert("Type something to translate")
        }
        //if more than 40 characters
        else if(inputText.length>40){
            alert("Not more than 40 characters at the time")
        }
        //if not only letters
        else if(!/^[A-Za-z\s]*$/.test(inputText)){
            alert("Only letters and spaces")
        }
        else{
            //set the right images and post to users translations in api
            translation()  
            PostTranslation(
            inputText, us.user.username) //get user from local storage
        }
    }
    //To handle change in input
    //Input is in inputText
    const HandleInput = event =>{
        setInputText(event.target.value)
    }
    //to set images
    const translation = () =>{
        setImage(inputText.split("").map(n=> `./LostInTranslation_Resources/individial_signs/${n}.png`))
    }
    return(
        <>
        <header>
            <img src="./LostInTranslation_Resources/Logo.png" id="logoImg"></img>
            <NavLink exact to="/profile">
                <img src="./LostInTranslation_Resources/profileImg.png" id="profileImg"></img>
            </NavLink>
            <h1>Lost in translations</h1>
        </header>
        <div>
            <input type="text" 
                placeholder="Enter words to translate"
                onChange={HandleInput}>
            </input><br/>
            <button onClick={ClickTranslateBtn}>Translate</button>
        </div>

        <div id="translatedContainer">
            {   
                image && image.map((e,i)=>{
                    //console.log(e) They must have an individual key! 
                    return <img src={e} key={i}></img>
              })
            }
        </div>
        </>
    )
}
//export default Translation
export default  withAuth(Translation)

