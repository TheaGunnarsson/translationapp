
import withAuth from "../hoc/withAuth"
import { useEffect, useState } from "react";
import { deleteTranslation } from "../api/index.js"
import { useUser } from '../context/UserContext'

const Profile = () => {
    const [usersTranslations, setUsersTranslations] = useState([]);
    const us = useUser();
    const { user, setUser } = useUser()
    //to load data
    useEffect(() => {
        handleLoadtranslationData();
    }, []);

        //to get the ten translations
    const handleLoadtranslationData = async () => {
        console.log(us)
        const response = await fetch(
            `https://api-assignment-jt.herokuapp.com/translations?username=${us.user.username}`
        );
        const result = await response.json();
        //only taking ten of them
        var onlyTen = result[0].translations.slice(0, 10)
        setUsersTranslations(onlyTen); 
        
    };
    //to add every item in element 
    const models = usersTranslations.map((ut, index) => { 
            return (
                <div key={index}>
                    <p>{ut}</p>
                </div>
              );
      });
    
    //does not refresh the page
    //do overwrite deletes in api
    const ClickToDelete = () => {
        let sliceNum = usersTranslations.length;
        const response = fetch(
            `https://api-assignment-jt.herokuapp.com/translations?username=${us.user.username}`
        )
        .then(response => response.json())
        .then(test =>{
            //console.log(test)
            console.log(usersTranslations)
            const toPatch = test[0].translations.slice(sliceNum)
            //console.log(toPatch)
            deleteTranslation(toPatch, usersTranslations, us.user.username)
        })
    }
    const ClickToLogOut = () => {
        //clear storage and redirect to start
        setUser(null)
    }
    return(
        <>
        <h1>Profile for {us.user.username}</h1>
        <div id="translationsContainer">
            {usersTranslations.length>0 ? models: <p>No translations</p>}
        </div>
        <button onClick={ClickToDelete}>Delete all</button>
        <button onClick={ClickToLogOut}>Log out</button>
        </>
    );
};

//export default Profile
export default  withAuth(Profile)

