
const apiUrl = 'https://api-assignment-jt.herokuapp.com/translations';
const apiKey = "apiKey"


const checkForUser = async (username) => {
    try{
        const response = await fetch(`${apiUrl}?username=${username}`)
        if(!response.ok){
            throw new Error("Could not complete request")
        }
        const data = await response.json() //Get the data from the response
        return [null, data]
    }
    catch (error){
        return [error.message, []]
    }
}

const createUser = async (username) => {
    try{
        const response = await fetch(apiUrl, {
            method: "POST", //Create a resource
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok){
            throw new Error("Could not create user with username" + username)
        }
        const data = await response.json() //Get the data from the response
        return [null, data] //Returns null (error) or a user
    }
    catch (error){
        return [error.message, []] //Returns error message or empty array
    }
}

export const loginUser = async username => {
    const [ checkError, user ] = await checkForUser(username)

    if(checkError !== null){
        return [ checkError, null ] //Returns an error or null
    }

    if(user.length > 0){
        return [ null, user.pop() ] //Returns null (error) or a user
    }
    return await createUser(username)

    
}