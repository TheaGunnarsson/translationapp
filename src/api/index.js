const apiKey = "apiKey"
const apiURL = 'https://api-assignment-jt.herokuapp.com/translations';

//To post users translations to api
export const PostTranslation = async (data, username) => {
    //to first get the users id
    const response = await fetch(`${apiURL}?username=${username}`)
    const users = await response.json(); 
    const userId = users[0].id;
    const previusTranslations = users[0].translations;
    //pushing the new translation to array with previus translations
    previusTranslations.push(data)

    await fetch(`${apiURL}/${userId}`, {
        method: 'PATCH',
        headers: {
            'X-API-Key': apiKey,
            'Content-Type': 'application/json'
        },
        //to keep previus translations, will not be able to mark them as deleted
        body: JSON.stringify({translations: previusTranslations}) 
    })
}
//deletes post from users translations and puts them into users deleted
export const deleteTranslation = async (data, deteted, user) => {

     const response = await fetch(`${apiURL}?username=${user}`)
     const users = await response.json(); 
     const userId = users[0].id;
    
     //updating translations
     await fetch(`${apiURL}/${userId}`, {
         method: 'PATCH',
         headers: {
             'X-API-Key': apiKey,
             'Content-Type': 'application/json'
         },
         body: JSON.stringify({translations: data}) 
    })
    //to add deleted array, maybe overwrites with several deletes
    //should do as posttranslation and add them together
    await fetch(`${apiURL}/${userId}`, {
        method: 'PATCH',
        headers: {
            'X-API-Key': apiKey,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({"deleted": deteted}) 
   })
}

