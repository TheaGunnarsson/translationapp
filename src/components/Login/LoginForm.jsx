//Import everything needed to make login work
import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/StorageKeys'

const usernameConfig = {
    required: true, //Username is required to log in
    minLength: 3 //Min length of the username is 3 symboles
}

const LoginForm = () => {
    //Hooks
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    //Local State
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)

    useEffect(() => {
        if(user !== null){ //If an accepted username is entered
            navigate('translations')
        }
    }, [ user, navigate ]) //When a user is logged in take them to the translation page
        
    const onSubmit = async ({ username }) => {
                setLoading(true);
               const [error, userResponse] = await loginUser(username);
               if(error !== null){
                   setApiError(error)
               }
               if(userResponse !== null){
                   storageSave(STORAGE_KEY_USER, userResponse)
                   setUser(userResponse)
               }
               setLoading(false);
    };

        //Render Function
        const errorMessage = (() => { //Check if there is something wrong with the entered username
            if(!errors.username) { //If nothing is wrong
                return null
            }
            if(errors.username.type === "required"){ //If button is clicked without entering a username
                return <span>Username is required</span>
            }
            if(errors.username.type === "minLength"){ //If button is clicked but its too short
                return <span>Username is too short</span>
            }
        })()

    return (
        <>
        <h2 style={{backgroundColor: " #fad462", padding: "10px 50px"}}>What is your name?</h2>
        <form onSubmit = { handleSubmit(onSubmit) }> 
            <fieldset style={{backgroundColor: "#fad462", textAlign: "center"}}>
                <label htmlFor = "username">Username:</label>
                <input 
                type = "text" 
                placeholder = "Enter your name" //Placeholder text in input field
                {...register("username", usernameConfig)} />
                { errorMessage }
            </fieldset>
            {/* The button to log in, when pressed it gets disabled and loggs you in to translations */}
            <button type = "submit" disabled={ loading } style={{backgroundColor: "#b19bea", padding: "15px 35px", cursor: "pointer", borderRadius: "12px", marginLeft: "45%"}}>Log in</button>
          
            { loading && <p>Logging in...</p>} {/* When button is pressed show this to show something is happening */}
            { apiError && <p>{ apiError }</p> }
        </form>
        </>
    )
}
export default LoginForm
