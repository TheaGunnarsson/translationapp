import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

const withAuth = Component => props => {
    const { user } = useUser()
    if(user !== null){ //If a user exist stay on the page
        return <Component {...props}/>
    }
    else {
        return <Navigate to="/" /> //If user dont exist take them back to the login page
    }
}
export default withAuth