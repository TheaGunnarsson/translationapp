import { createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/StorageKeys";
import { stoarageRead } from "../utils/storage";

const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext) //Returns user and setUser
}

const UserProvider = ({ children }) => {

    const [ user, setUser ] = useState(stoarageRead(STORAGE_KEY_USER))

    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={ state }>
            { children }
        </UserContext.Provider>
    )
}
export default UserProvider